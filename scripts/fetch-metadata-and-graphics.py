#!/usr/bin/env python3

import google_play_scraper
import os
import requests
import yaml
from pathlib import Path
import time

HEADERS = {'User-Agent': 'appcheck.mobilsicher.de'}


def download_file(url, local_filename):
    # the stream=True parameter keeps memory usage low
    r = requests.get(url, stream=True, allow_redirects=True, headers=HEADERS)
    r.raise_for_status()
    content_type = r.headers['Content-Type'].lower()
    if content_type == 'image/png':
        local_filename = local_filename.with_suffix('.png')
    elif content_type == 'image/jpeg':
        local_filename = local_filename.with_suffix('.jpeg')
    else:
        print('ERROR: mystery Content-Type', content_type)
        exit(1)
    with open(local_filename, 'wb') as f:
        for chunk in r.iter_content(chunk_size=1024):
            if chunk:  # filter out keep-alive new chunks
                f.write(chunk)
                f.flush()


apps = [
    ["ARD Audiothek", "de.ard.audiothek"],
    ["ARD Mediathek", "de.swr.avp.ard"],
    ["Arte", "tv.arte.plus7"],
    ["AusweisApp2", "com.governikus.ausweisapp2"],
    ["Beihilfe NRW", "de.ibm.com.ish.prod.beihilfenrw"],
    ["Corona Warn App", "de.rki.coronawarnapp"],
    ["DB Navigator", "de.hafas.android.db"],
    ["Deutschlandfunk", "de.deutschlandradio.dlf24"],
    ["Fahrplaner", "de.hafas.android.vbn"],
    ["HandyParken München", "de.swm.parken.handyparken"],
    ["KATWARN", "de.combirisk.katwarn"],
    ["MVG Deutschland", "de.mvg.mvgdeutschland"],
    ["MVG Fahrinfo", "de.swm.mvgfahrinfo.muenchen"],
    ["Meldoo - Mängelmelder für deine Stadt", "de.meldooplus.meldoo"],
    ["NINA - Warnapp", "de.materna.bbk.mobile.app"],
    ["Onleihe", "de.etecture.ekz.onleihe"],
    ["Ordnungsamt Online", "de.berlin.ordnungsamtonline"],
    ["Post & DHL", "de.dhl.paket"],
    ["Stadt Köln - offizielle App", "de.stadtkoeln.koelnapp"],
    ["Transakt", "com.entersekt.android.transakt"],
    ["VR SecureGo plus Zahlungen direkt freigeben", "de.fiduciagad.securego.vr"],
    ["WarnWetter", "de.dwd.warnapp"],
    ["ZDFmediathek & Live TV", "com.zdf.android.mediathek"],
    ["eBeihilfe Hessen", "sinc.cases.beihilfe.hessen"],
    ["nora - Notruf App", "de.noranotruf"],
    ["tagesschau", "de.tagesschau"],
]

# map Google Play fields to F-Droid metadata fields
# https://f-droid.org/docs/Build_Metadata_Reference/
fields = [
    ('developer', 'AuthorName'),
    ('developerEmail', 'AuthorEmail'),
    ('developerWebsite', 'AuthorWebSite'),
]
# map Google Play fields to fastlane-style localization files
# https://f-droid.org/docs/All_About_Descriptions_Graphics_and_Screenshots/#in-the-f-droid-repo
files = [
    ('description', 'description.txt'),
    ('summary', 'summary.txt'),
    ('title', 'name.txt'),
]

os.chdir(Path(__file__).parent.parent)

metadatadir = Path('metadata')
for name, appid in apps:
    print(name, appid)
    metadata = {'AutoName': name}
    for lang, country in (('en', 'US'), ('de', 'DE')):
        result = google_play_scraper.app(appid, lang=lang, country=country)
        for key, field in fields:
            if result.get(key):
                metadata[field] = result[key]
        if result.get('adSupported') or result.get('containsAds'):
            metadata['AntiFeatures'] = ['Ads']

        locale = '%s-%s' % (lang.lower(), country.upper())
        localedir = metadatadir / appid / locale
        for key, filename in files:
            outfile = localedir / filename
            outfile.parent.mkdir(parents=True, exist_ok=True)
            with open(outfile, 'w') as fp:
                fp.write(result[key])
                fp.write('\n')

        # https://developers.google.com/people/image-sizing
        # https://gist.github.com/Sauerstoffdioxid/2a0206da9f44dde1fdfce290f38d2703
        url = result.get('headerImage')
        if url:
            url += '=h1024-w500-nu'
            download_file(url, localedir / 'featureGraphic')
        url = result.get('icon')
        if url:
            url += '=h512-w512-nu'
            download_file(url, localedir / 'icon')

        screenshotsdir = localedir / 'phoneScreenshots'
        screenshotsdir.mkdir(parents=True, exist_ok=True)
        for url in result.get('screenshots', []):
            url += '=h0-nu'
            name = os.path.basename(url)
            download_file(url, screenshotsdir / name[: name.rfind('=')])

        os.system('rdfind -deleteduplicates true ' + str(localedir.parent.resolve()))

    with open('metadata/%s.yml' % appid, 'w') as fp:
        yaml.dump(metadata, fp, default_flow_style=False, allow_unicode=True)
    time.sleep(1)  # prevent Google's servers from throttling
