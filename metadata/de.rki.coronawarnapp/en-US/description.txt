The Robert Koch Institute (RKI) – in its capacity as the central federal institution for public health and as the German national public health institute – publishes the Corona-Warn-App on behalf of the German government. The app serves as a digital complement to distancing, hygiene and wearing masks. Additionally, it provides a functionality to add your digital vaccination certificates to prove your vaccination status. It uses Bluetooth technology and the Google Exposure Notification APIs. The app is designed to help break infection chains by informing you if you were recently close to someone who subsequently tested positive for corona. However, at no time does it collect any personal information. Who you are and where you are remain secret – and your privacy is well-protected.

HOW THE APP WORKS

Exposure logging is the core of the app and should always be active. When enabled, smartphones exchange encrypted random IDs with other devices using Bluetooth. 

The random IDs only provide information about the duration and distance of an encounter. No one is able to identify the person behind these IDs. The Corona-Warn-App does not collect any information about the location of the encounter or that of its users.

Based on maximum corona incubation times, the random IDs your smartphone collects are stored in an exposure log for 14 days and then deleted. If a person is diagnosed with corona and chooses to share his/her own smartphone’s random IDs, all persons previously encountered will then be notified anonymously.

No one will know when, where or with whom the exposure event took place. The infected person remains anonymous.

In addition to the exposure notifications, affected users will receive clear recommendations for action. Important: Information about the notified persons is not accessible at any time.

HOW YOUR DATA IS PROTECTED

The Corona-Warn-App is designed to be your daily companion, without knowing your identity. This is why the app cannot tell anyone who you are. Data protection is fully guaranteed throughout the app’s entire service life.

- No registration required: No email address and no name need to be submitted.

- No personal information is transmitted: When you encounter another person using the app, your smartphones only exchange random IDs. They measure the time and distance of an encounter, but do not allow to identify persons or find out locations.

- Decentralized storage: Data is stored only on the smartphone itself and deleted after 14 days.

- No third-party access: Neither the person reporting a proven corona infection, nor those who have been notified can be traced – not by the German government, the Robert Koch Institute, any other user, nor by Google.

This app is not intended for any use outside of Germany. Corona-Warn-App is the main corona app for Germany, where it is interconnected with the national health care system. Despite of the above, Corona-Warn-App is also available in this country. It is provided for anyone living, working, vacationing, or visiting Germany regularly or for an extended period of time.

The terms of use for the Corona-Warn-App apply: https://www.coronawarn.app/assets/documents/cwa-eula-en.pdf. By installing and using this app, you accept these terms of use.
