Die neue tagesschau-App liefert Ihnen die wichtigsten und aktuellen Nachrichten des Tages - direkt auf Ihr Smartphone oder Tablet!

Im neuen Story-Modus swipen Sie für einen schnellen News-Überblick ganz intuitiv durch die aktuellsten Top-Schlagzeilen von heute - aus Deutschland und der Welt.

Im Bereich „News“ finden Sie alle Nachrichten der tagesschau geordnet nach wichtigen Ressorts wie z.B. Ausland, Inland, Wirtschaft (inkl. Börse), Investigativ oder Wetter. Unter „Meine Regionen“ können Sie sich zudem über regionale Nachrichten aus Ihrem Bundesland* informieren. Die wichtigsten Nachrichten finden Sie außerdem auch als Video.

Unter „Sendungen“ („TV") finden Sie den aktuellen Livestream laufender oder Videos vergangener Sendungen u.a. der tagesschau (auch mit Gebärdensprache), der tagesthemen, vom nachtmagazin oder von tagesschau24. Für Ihr schnelles News-Update unterwegs eignet sich die bewährte „tagesschau in 100 Sekunden“!

Erhalten Sie optional alle Eilmeldungen der tagesschau-Redaktion per Push-Nachricht auf Ihr Gerät – erfahren Sie live, wenn etwas Wichtiges passiert! Die App verfügt jetzt auch über einen "Dark Mode" (ab Android 10 nutzbar).

In der App finden Sie aktuelle Nachrichten der ARD (Das Erste: BR, hr, mdr, NDR, radiobremen, rbb, SR, SWR, WDR) und der Sportschau. Unsere App bietet Ihnen das Beste aus dem weltweiten Korrespondentennetz der ARD!

Die tagesschau-App und ihre Inhalte erhalten Sie kostenlos. Für den Abruf des Livestreams und der Videos aus Mobilfunknetzen empfehlen wir eine Flatrate, da sonst höhere Verbindungskosten anfallen können.

Wir bedanken uns bei unseren mehr als zwei Millionen - oft langjährigen - Nutzer*innen und freuen uns im Play Store über Ihre Bewertung, Ihr Lob, Ihre Kritik und Ihre Anregungen!

Wir haben jetzt übrigens auch eine tagesschau-App für AndroidTV.

Viele Grüße aus Hamburg & Leipzig vom Team der tagesschau-App


*Die App enthält aktuelle Nachrichten aus:

Bayern, Baden-Württemberg, Berlin, Brandenburg, Bremen, Hamburg, Hessen, Mecklenburg-Vorpommern, Niedersachsen, Nordrhein-Westfalen, Rheinland-Pfalz, Saarland, Sachsen, Sachsen-Anhalt, Schleswig-Holstein, Thüringen
