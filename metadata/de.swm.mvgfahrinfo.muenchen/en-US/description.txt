The MVG Fahrinfo München app offers everything that public transport users in and around Munich need to get from A to B. You can use the app to quickly plan a journey and buy the right ticket.

<h1>FEATURES</h1>
<b>★ All journeys</b>: Based on the timetable, real-time predictions and reported incidents.

<b>★ All departures</b>: With real time prediction including filters for your own line and information on your stop.

<b>★ Tickets for the entire MVV network</b>: Including ticket widget for even faster purchasing of favourite tickets.

<b>★ SUBSCRIPTION AS MOBILE TICKET</b>: The personalised MVV subscription is available as a HandyTicket.

<b>★ Filter for accessible journeys</b>: For stress-free mobility with wheelchair, walking frame or buggy.

<b>★ Reminder function</b>: So you get to the stop on time.

<b>★ Better orientation</b>: with Integrated city map and location function.

<b>★ Push notifications for incidents</b>: Information on underground, buses, trams, suburban trains and MVV regional buses.

<b>★ All network maps</b>: For connections in Munich, the region, or trains in Bavaria. Plus: network maps for accessible mobility.

<b>★ Personal mobility made easy</b>: Information on MVG Bike and car-sharing options at all stops. You can easily access bikes and sharing partners via smart link to our app MVG more.

<b>★ More flexible mobility</b>: Links to our partners MVG Nightline, park & ride, taxi services as well as our mobility partners S-Bahn München, Bayerische Regiobahn, alex, Meridian and FlixBus.

<b>★ Event tips</b>: for selected events and smart link to the official Munich App of the City of Munich.

<b>★ Customer service at the tap of a button</b>: Details on the customer hotline as well as the Customer Service Centres and lost property office.

<h1>OUR TIPS</h1>
<b>💡 Easy address search</b>One or two syllables of the street name + house number are sufficient, e.g. “<i>Kuni 72</i>” for “<i>Kunigundenstrasse 72, Munich</i>”.

<b>💡 Abbreviations</b>“<i>MUC</i>” = Munich Airport, “<i>Hbf</i>” = Munich Central Station (Hauptbahnhof), “<i>OEZ</i>” = Olympia Shopping Centre (Olympiaeinkaufszentrum).

<b>💡 In the event of unusual results, reset settings</b>
Settings icon top right ➔ “Settings” ➔ “Reset” (next to the “?”)

<b>💡 Where is stop position 4?</b>Tap on the stop name in the journey details/departures and load the plan of the stop. The number tells you from which point exactly your line departs.
<i>Note</i>: available for all underground stations and for stops with multiple stopping points.

<b>💡 Show footpath on the map</b>:simply tap on the footpath symbol to the stop or your destination in the journey details.

<b>💡 Buy a required add-on ticket</b>:
Save the tariff zones of your season ticket under “<i>Settings</i>“ ➔ “<i>My Zones</i>“.

<b>💡 Explain functions </b>:By tapping on the “<i>?</i>” in the bar on the top right, additional information on the optimal use of the app can be found.

<h1>NOTE</h1>
★ Purchased mobile tickets can be shown at any time, even without an internet connection.
★ No guarantee can be given for the accuracy or completeness of the provided information.
★ HandyTicket gültig im gesamten MVV Gebiet z. B. in:
<b>A</b>llershausen, Andechs, Aying, <b>B</b>erg, Brunnthal, <b>D</b>achau, Dorfen, <b>E</b>bersberg, Eching, Erding, <b>F</b>reising, <b>G</b>arching, Gauting, Gilching, Grafing, Gröbenzell, Grünwald, <b>H</b>aar, Hallbergmoos, Herrsching, Holzkirchen, <b>I</b>smaning, <b>J</b>esenwang, <b>K</b>arlsfeld, Keferloh, Kirchseeon, Krailling, <b>L</b>ochham, <b>M</b>aisach, Mammendorf,  Martinsried, Marzling, Moosburg, <b>N</b>eubiberg, Neufahrn, <b>O</b>berschleißheim, Olching, Ottobrunn, <b>P</b>etershausen, Planegg, Poing, Putzbrunn, <b>R</b>öhrmoos, <b>S</b>auerlach, Schäftlarn, Starnberg, <b>T</b>aufkirchen, Tutzing, <b>U</b>nterhaching, <b>V</b>aterstetten, <b>W</b>asserburg, <b>Z</b>orneding
