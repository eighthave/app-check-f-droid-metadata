ARD Audiothek – Jederzeit das Beste hören

Hören Sie, was Sie wollen, wann Sie wollen. Ob Hörspiel, Wissen, Dokumentation oder Comedy - mit der ARD Audiothek kommen die Premium-Inhalte aus den Radioprogrammen der ARD und des Deutschlandradios direkt auf Ihr Smartphone.

Entdecken – On Demand und Live
Ob Hörspiele, Krimis, Comedy, Dokus, Reportagen, Wissen oder Geschichte: Täglich neu präsentiert Ihnen unsere Redaktion die besten Sendungen und Geheimtipps. Ob thematische Sammlungen oder Highlights aus dem Angebot der ARD-Radiosender: Unter Entdecken finden Sie immer etwas, das Sie interessiert. Auch die Livestreams der ARD-Radiosender können Sie in der ARD Audiothek abrufen.

Was Sie wollen, wann Sie wollen – Ihr persönliches Hörerlebnis
Stöbern Sie in den Rubriken oder nutzen Sie die Suche, um genau die Inhalte zu finden, die Sie interessieren. Sie können Sendungen abonnieren, Audios vormerken und eigene Playlists erstellen. Über den ARD-Login können Sie diese Inhalte in Ihrem Konto speichern und so auch geräteübergreifend nutzen. Lassen Sie sich von Empfehlungen inspirieren und teilen Sie über die Sharing-Funktion mit Freunden, was Sie begeistert!

Jederzeit und überall – Die ARD Audiothek auch unterwegs nutzen
Kein mobiles Internet? Kein Problem. Sie können Audios einfach offline speichern und unterwegs nachhören. Mit Android Auto können Sie die ARD Audiothek mit dem Android-System Ihres Autos verbinden und auf jeder Fahrt hören, was Sie möchten. Sie können die Wiedergabe auch auf externe Geräte übertragen.

Mehr Funktionen
Die ARD Audiothek bietet noch viel mehr und wird stetig um neue Funktionen erweitert. Testen Sie den energiesparenden Dark Mode, sehen Sie die Liste ihrer bereits gehörten Audios oder nutzen Sie die zahlreichen Filterfunktionen und persönlichen Einstellungen. Schauen Sie es sich selbst an und hören Sie rein!
