Unsere ServiceApp bietet Ihnen mobil einen gezielten und themenorientierten Zugriff auf die Informationen der Stadt Köln.

Machen Sie die App zu Ihrer App!

Die neue ServiceApp 2.0 bindet mit einem modernen Design die bestehenden Inhalte der mobilen Webseite „www.stadt-koeln.de“ ein. Übersichtliche Kacheln ermöglichen Ihnen direkten Zugriff auf relevante Themen und Lebenslagen. Durch Ein- und Ausblenden der Kacheln können Sie die Startseite individuell gestalten. Außerdem können Sie sich für häufig benötigte Informationen Favoriten anlegen.

Über die Anwendung „Sag´s uns“ melden Sie direkt von Ihrem Standort wilden Müll, Fahrradleichen, defekte Ampeln, verstopfte Gullys und mehr.

Drei „Live-Kacheln“ zeigen Ihnen auf einen Blick das Kundenzentrum mit der geringsten Wartezeit, den aktuellen Rheinpegel sowie das Parkhaus in der Innenstadt mit den meisten freien Plätzen.

Eilmeldungen der Stadt werden automatisiert auf der Startseite eingeblendet. Die App informiert Sie daher schnell über aktuelle Ereignisse, wie beispielsweise Bombenentschärfungen.

Mit neuen Themenkacheln erweitern wir die Funktionen der App bei Bedarf, ohne dass eine Neuinstallation für Sie notwendig wird.
